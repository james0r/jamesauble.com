import './assets/scss/app.scss';
import './assets/js/main';

var jquery = require("jquery");
window.$ = window.jQuery = jquery; // notice the definition of global variables here
require("jquery-ui-dist/jquery-ui.js");

module.exports.sayHello = function sayHello() {
    console.log("hello mom");
}

module.exports.scrollToContact = function scrollToContact() {
    $('.contact-section').scrollintoview({
        duration: 'slow'
    });
}
module.exports.scrollToAbout = function scrollToAbout() {
    $('.about-section').scrollintoview({
        duration: 'slow'
    });
}
module.exports.scrollToSkills = function scrollToSkills() {
    $('.skills-section').scrollintoview({
        duration: 'slow'
    });
}
module.exports.scrollToPortfolio = function scrollToPortfolio() {
    $('.portfolio-section').scrollintoview({
        duration: 'slow'
    });
}