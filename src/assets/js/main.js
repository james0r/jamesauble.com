import './jquery.slicknav';
import './jquery.waypoints.min.js';

var jquery = require("jquery");
window.$ = window.jQuery = jquery; // notice the definition of global variables here
require("jquery-ui-dist/jquery-ui.js");

//Ready hook to add sticky header when page is scrolled at all.
$(document).ready(function () {
  if (/Edge?/.test(window.navigator.userAgent)) {
    console.log("Client is Microsoft Edge. Static image used in place of SVG animation.");
    $('.preloader-container')
      .css('display', 'none');
  }

  //Click handler for return to top button.

  $('#rtt-btn').on('click', function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  })

  $(window).scroll(function () {
    //Print distance from top of page to the console.
    if ($(window).scrollTop() > 0) {
      $('#sticky-header').addClass('navbar-fixed');
      $('#sticky-header-inner').addClass('navbar-fixed-inner');
    }
    if ($(window).scrollTop() < 100) {
      $('.header').removeClass('navbar-fixed');
    }

    //Once scrolled past screen height make return to top button visible

    if ($(window).scrollTop() > $(window).height() && $(window).width() > 768) {
      $('#rtt-btn').addClass('rtt-btn__visible');
    }

    if ($(window).scrollTop() < $(window).height()) {
      $('#rtt-btn').removeClass('rtt-btn__visible');
    }
  });
});

//Initialize mobile menu for normal header
$(function () {
  $('#mainmenu').slicknav({
    appendTo: '#header',
    label: ''
  });
});

//Initialize mobile menu for sticky header.
$(function () {
  $('#sticky-mainmenu').slicknav({
    appendTo: '#sticky-header',
    label: ''
  });
});

//Handle click events from hamburger icon for normal and sticky menu
$('.nav-mobile-icon').click(function () {
  console.log("toggleMobileMenu called");
  $('#mainmenu').slicknav('toggle');
})
$('.sticky-nav-mobile-icon').click(function () {
  console.log("toggleMobileMenu called");
  $('#sticky-mainmenu').slicknav('toggle');
})

//Handle click event from 'Show Other Technologies Used' button in skills and expand section.
$('.show-more-skills-btn').click(function () {
  $('.show-more-skills-btn').hide();
  $('.expanded-skills-item').fadeIn();
})

$('.hide-more-skills-btn').click(function () {
  $('.expanded-skills-item').hide();
  $('.show-more-skills-btn').show();
  Window.scrollToSkills();
})

$.fn.scrollView = function () {
  return this.each(function () {
    let offset;
    if ($(".slicknav_menu").css('height') == '0px' || $(window).scrollTop() > 100 ) {
      offset = 75;
    } else {
      offset = 225;
    }
    $('html, body').animate({
      scrollTop: $(this).offset().top - offset
    }, 1000);
  });
}

Window.scrollToPortfolio = function() {
  $('.portfolio-section').scrollView();
  clearActiveLinks();
}
Window.scrollToSkills= function () {
  $('.skills-section').scrollView();
  clearActiveLinks();
}
Window.scrollToContact = function () {
  $('.contact-section').scrollView();
  clearActiveLinks();
}
Window.scrollToAbout = function () {
  $('.about-section').scrollView();
  clearActiveLinks();
}


//Waypoints section to handle active link styling

function clearActiveLinks() {
  $('a').removeClass("nav-link-active");
}

//Helper method to clear all other active links before setting a new one

$('#logo-img').waypoint(function () {
  clearActiveLinks();
})

//Different waypoints for up and down direction to make sure active
//link triggers after scrollTo

$('.portfolio-section').waypoint({
  handler: function (direction) {
    if (direction == 'down') {
      clearActiveLinks();
      $('.portfolio-link').addClass('nav-link-active');
    }
  },
  offset: 80
})
$('.portfolio-section').waypoint({
  handler: function (direction) {
    if (direction == 'up') {
      clearActiveLinks();
      $('.portfolio-link').addClass('nav-link-active');
    }
  },
  offset: 70
})
$('.about-section').waypoint({
  handler: function (direction) {
    if (direction == 'down') {
      clearActiveLinks();
      $('.about-link').addClass('nav-link-active');
    }
  },
  offset: 80
})
$('.about-section').waypoint({
  handler: function (direction) {
    if (direction == 'up') {
      clearActiveLinks();
      $('.about-link').addClass('nav-link-active');
    }
  },
  offset: 70
})
$('.skills-section').waypoint({
  handler: function (direction) {
    if (direction == 'down') {
      clearActiveLinks();
      $('.skills-link').addClass('nav-link-active');
    }
  },
  offset: 80
})
$('.skills-section').waypoint({
  handler: function (direction) {
    if (direction == 'up') {
      clearActiveLinks();
      $('.skills-link').addClass('nav-link-active');
    }
  },
  offset: 70
})
$('.contact-section').waypoint({
  handler: function (direction) {
    if (direction == 'down') {
      clearActiveLinks();
      $('.contact-link').addClass('nav-link-active');
    }
  },
  offset: 80
})
$('.contact-section').waypoint({
  handler: function (direction) {
    if (direction == 'up') {
      clearActiveLinks();
      $('.contact-link').addClass('nav-link-active');
    }
  },
  offset: 70
})